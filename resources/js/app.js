/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('chat-messages', require('./components/ChatMessages.vue').default);
Vue.component('chat-form', require('./components/ChatForm.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#app',
    data: {
        messages: [],
        chats: [],
        room: {},
        room_id: null,
    },

    created() {
        const path = window.location.pathname;

        const roomId = path.substring(path.lastIndexOf('/') + 1);

        if(roomId !== 'chats'){
            this.fetchRoom(roomId);
        }
    },

    methods: {
        fetchMessages(room) {
            axios.get('/messages/' + room ).then(response => {
                this.messages = response.data;
            });
        },

        fetchRoom(name) {
            axios.get('/room/' + name ).then((response) => {
                this.room = response.data;

                this.fetchMessages(this.room.name)

                if(this.room.is_presence){
                    Echo.join(`chat.presence.${this.room.name}`)
                        .here((users) => {
                            this.chats  = users
                        })
                        .joining((user) => {
                            this.chats.push(user)
                        })
                        .leaving((user) => {
                            console.log(user)
                            // this.chats.reduce(user)
                        })
                        .error((error) => {
                            console.error(error, 'error');
                        })
                        .listen('MessagePresenceSent', (e) => {
                            console.log(e)
                            this.messages.push({
                                message: e.message.message,
                                is_private: e.is_private,
                                user: e.user
                            });
                        });
                }

                else if(this.room.is_private){
                    Echo.private(`chat.private.${this.room.name}`)
                        .listen('MessagePrivateSent', (e) => {
                            console.log(e)
                            this.messages.push({
                                message: e.message.message,
                                is_presence: e.is_presence,
                                user: e.user
                            });
                        });
                }
                else{
                    Echo.channel(`chat.general`)
                        .listen('MessageGeneralSent', (e) => {
                            this.messages.push({
                                message: e.message.message,
                                is_publish: e.is_publish,
                            });
                        });
                }
            });
        },

        addMessage(message) {
            this.messages.push(message);

            axios.post('/messages', message).then(response => {
            });
        }
    }
});
