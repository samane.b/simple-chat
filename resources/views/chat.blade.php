@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <h2 class="panel-heading">Chats</h2>

            <hr/>

            <div class="panel-body">
                <chat-messages :messages="messages" :chats="chats" :room="{{ $room }}"></chat-messages>
            </div>
            <div class="panel-footer">
                <chat-form
                    v-on:messagesent="addMessage"
                    @if(Auth::check()) :user="{{ Auth::user() }}" @endif
                    :room="{{ $room }}"
                ></chat-form>
            </div>
        </div>

    </div>
@endsection
