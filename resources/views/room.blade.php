@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 p-5 pt-2">
                <h3><i class="fas fa-tachometer-alt ml-2"></i>Room</h3><hr>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Date</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rooms as $room)
                            <tr>
                                <td>{{ $room->id }}</td>
                                <td>{{ \Illuminate\Support\Str::title($room->name) }}</td>
                                <td>{{ $room->created_at->format('Y M D') }}</td>
                                <td>
                                    <div class="btn-group btn-group-sm">
                                        <a href="/chats/{{$room->name}}"  class="btn btn-primary">Join</a>

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-10 p-5 pt-2">
                <h3><i class="fas fa-tachometer-alt ml-2"></i>User</h3><hr>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Date</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ \Illuminate\Support\Str::title($user->name) }}</td>
                                <td>{{ $user->created_at->format('Y M D') }}</td>
                                <td>
                                    <div class="btn-group btn-group-sm">
                                        <a href="/chats/user/{{$user->id}}"  class="btn btn-primary">Private Message</a>

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
