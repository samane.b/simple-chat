<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/chats');
});

Auth::routes();

Route::get('/room/{room:name}', [\App\Http\Controllers\ChatsController::class, 'room']);
Route::get('/chats', [\App\Http\Controllers\ChatsController::class, 'index']);
Route::get('/chats/{room:name}', [\App\Http\Controllers\ChatsController::class, 'show']);
Route::get('/chats/user/{id}', [\App\Http\Controllers\ChatsUserController::class, 'show']);
Route::get('/messages/{room:name}', [\App\Http\Controllers\ChatsController::class, 'fetchMessages']);
Route::post('/messages', [\App\Http\Controllers\ChatsController::class, 'sendMessage']);
