<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/



Broadcast::channel('chat.presence.{roomId}', function ($user, $roomId) {
    return [
        'user' => $user,
        'room' => \App\Models\Room::query()->where('name', $roomId)->first(),
    ];
});

Broadcast::channel('chat.private.{roomId}', function ($user, $roomId) {
    return [
        'user' => $user,
        'room' => \App\Models\Room::query()->where('name', $roomId)->first(),
    ];
});

Broadcast::channel('chat.general', function ($user, $roomId) {
    return [
        'user' => $user,
        'room' => \App\Models\Room::query()->where('name', $roomId)->first(),
    ];
});
