<?php

namespace Database\Seeders;

use App\Models\Room;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Room::factory()->create([
            'name' => 'music',
            'is_private' => false,
            'is_presence' => true,
        ]);

        Room::factory()->create([
            'name' => 'movie',
            'is_private' => false,
            'is_presence' => true,
        ]);

        Room::factory()->create([
            'name' => 'football',
            'is_private' => false,
            'is_presence' => true,
        ]);

        Room::factory()->create([
            'name' => 'love',
            'is_private' => false,
            'is_presence' => true,
        ]);

        Room::factory()->create([
            'name' => 'general',
            'is_private' => false,
            'is_presence' => false,
        ]);
    }
}
