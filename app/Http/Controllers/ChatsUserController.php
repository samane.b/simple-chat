<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ChatsUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $ids =  [Auth::id(), $id];

        sort($ids, SORT_ASC);

        $name = sha1(implode('-', $ids));

        $room = Room::query()->firstOrCreate(['name' => $name], [
            'uuid' => Str::uuid()
        ]);

        $room->update([
            'is_private' => true,
            'is_presence' => false,
        ]);

        return redirect('/chats/' . $room->name);
    }
}
