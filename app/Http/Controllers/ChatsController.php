<?php

namespace App\Http\Controllers;

use App\Events\MessageGeneralSent;
use App\Events\MessagePresenceSent;
use App\Events\MessagePrivateSent;
use App\Models\Message;
use App\Models\Room;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class ChatsController extends Controller
{/**
     * Show chats
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            $rooms = Room::query()->where('is_private', false)->where('is_presence', true)->get();
        }else{
            $rooms = Room::query()->where('is_private', false)->where('is_presence', false)->get();
        }
        $users = User::query()->whereNot('id', Auth::id())->get();

        return view('room')->with('rooms', $rooms)->with('users', $users);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(Room $room)
    {
        if(!Auth::check() && ($room->is_private || $room->is_presence)){
            abort(403);
        }
        return view('chat')->with('room',$room);
    }

    /**
     * Fetch all messages
     *
     * @return \Illuminate\Database\Eloquent\Collection|Message[]
     */
    public function fetchMessages(Room $room)
    {
        return Message::query()->where('room_id', $room->getKey())->with('user')->get();
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {
        $user = Auth::user();

        $room = Room::find($request->input('room.id'));

        if($user){
            $message = $user->messages()->create([
                'message' => $request->input('message'),
                'room_id' => $request->input('room.id')
            ]);
        }else{
            $message = Message::create([
                'message' => $request->input('message'),
                'room_id' => $request->input('room.id')
            ]);
        }

        if ($room->is_presence){
            MessagePresenceSent::broadcast($user, $message)->toOthers();
        }elseif ($room->is_private){
            MessagePrivateSent::broadcast($user, $message)->toOthers();
        }else{
            MessageGeneralSent::broadcast($user, $message)->toOthers();
        }

        return ['status' => 'Message Sent!'];
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function room(Room $room)
    {
        return $room;
    }
}
